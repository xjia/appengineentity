<?php
abstract class Entity
{
  private $datastore = NULL;
  private $entity = NULL;
  private $key = NULL;

  public function __construct($key = NULL)
  {
    $GAE = 'com.google.appengine.api.datastore.';

    $DatastoreServiceFactory = java_class($GAE . 'DatastoreServiceFactory');
    $this->datastore = $DatastoreServiceFactory->getDatastoreService();

    if (isset($key)) {
      $KeyFactory = java_class($GAE . 'KeyFactory');
      $this->key = $KeyFactory->createKey(get_class($this), $key);
      $this->entity = $this->datastore->get($this->key);
    } else {
      $this->entity = new Java($GAE . 'Entity', get_class($this));
    }
  }

  public function __set($name, $value)
  {
    $this->entity->setProperty($name, $value);
  }

  public function __get($name)
  {
    return $this->entity->getProperty($name);
  }

  public function __isset($name)
  {
    return $this->entity->hasProperty($name);
  }

  public function __unset($name)
  {
    $this->entity->removeProperty($name);
  }

  public function store()
  {
    $this->key = $this->datastore->put($this->entity);
  }

  public function trash()
  {
    if (isset($this->key)) {
      $this->datastore->delete($this->key);
    }
  }
}
